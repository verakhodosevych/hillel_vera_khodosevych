package com.task;

import com.task.animals_classes.Cat;
import com.task.animals_classes.Cow;
import com.task.animals_classes.Dog;
import com.task.animals_classes.Puppy;

public class Main {

    public static void main(String[] args) {
        Cat myCat = new Cat();
        Dog myDog = new Dog();
        Cow myCow = new Cow();
        Puppy myPuppy = new Puppy();

        myCat.speak();
        myDog.speak();
        myCow.speak();
        myPuppy.speak();
    }
}
