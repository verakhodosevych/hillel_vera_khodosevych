package com.task.animals_classes;

import com.task.abstract_class.Animal;

public class Dog extends Animal { //реализация принципа "Наследование"

    public void speak() {
    System.out.println("Bow-wow!");
    }
}
