package com.task.abstract_class;

public abstract class Animal { // абстракция

    protected abstract void speak(); // модификаторы доступа разных уровней
}
